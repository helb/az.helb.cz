import './style'
import { Component, h } from 'preact'
import lat2cyr from './lat2cyr'
import cyr2lat from './cyr2lat'

const convert = (text, lookup) => {
  lookup.forEach(pair => {
    text = text.replace(pair[0], pair[1])
  })
  return text
}

export default class App extends Component {
  constructor () {
    super()
    this.state = {
      lat: '',
      cyr: ''
    }
  }

  // eslint-disable-next-line no-undef
  convertCyr = event => {
    const text = event.target.value
    this.setState({ cyr: text, lat: convert(text, cyr2lat) })
  };

  // eslint-disable-next-line no-undef
  convertLat = event => {
    const text = event.target.value
    this.setState({ lat: text, cyr: convert(text, lat2cyr) })
  };

  render (props, { cyr, lat }) {
    return (
      <div>
        <textarea value={cyr} onInput={this.convertCyr} placeholder={'Ж'} />
        <textarea value={lat} onInput={this.convertLat} placeholder={'Ž'} />
      </div>
    )
  }
}
